package com.ewakom.mywork.jee.bookmark.dao;

import com.ewakom.mywork.jee.bookmark.db.DbConnector;
import com.ewakom.mywork.jee.bookmark.pojos.User;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Stateless
public class UserDAO {
    private static final String SELECT_ALL_QUERY = "SELECT id, login, password, name, surname, city, birth_year FROM bookmark.public.jee_user;";
    private static final String SELECT_BY_LOGIN_QUERY = "SELECT id, login, password, name, surname, city, birth_year FROM bookmark.public.jee_user WHERE login = ?";
    private static final String SELECT_BY_ID_QUERY = "SELECT id, login, password, name, surname, city, birth_year FROM bookmark.public.jee_user WHERE id = ?";
    private static final String UPDATE_USER_QUERY = "UPDATE bookmark.public.jee_user SET login = ?, password = ?, name = ?, surname = ?, city = ?, birth_year = ? WHERE id = ?";
    private static final String INSERT_USER_QUERY = "INSERT INTO bookmark.public.jee_user VALUES (?,?,?,?,?,?,?);";
    private static final String DELETE_USER_QUERY = "DELETE FROM bookmark.public.jee_user WHERE id = ?;";

    public List<User> getAllUsers() {
        List<User> users = null;
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_QUERY)) {
            users = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String login = rs.getString(2);
                String password = rs.getString(3);
                String name = rs.getString(4);
                String surname = rs.getString(5);
                String city = rs.getString(6);
                int birthYear = rs.getInt(7);
                User userFromRS = new User(login, password, name, surname, city, birthYear);
                userFromRS.setId(UUID.fromString(id));
                users.add(userFromRS);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return users;
    }


    public void registerUser(User user) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_USER_QUERY)) {
            ps.setObject(1, user.getId());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getName());
            ps.setString(5, user.getSurname());
            ps.setString(6, user.getCity());
            ps.setInt(7, user.getBirthYear());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void removeUser(UUID id) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_USER_QUERY)) {
            ps.setObject(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public User findUserByLogin(String login) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_BY_LOGIN_QUERY)) {
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String id = rs.getString(1);
                String password = rs.getString(3);
                String name = rs.getString(4);
                String surname = rs.getString(5);
                String city = rs.getString(6);
                int birthYear = rs.getInt(7);
                User userFromRS = new User(login, password, name, surname, city, birthYear);
                userFromRS.setId(UUID.fromString(id));
                return userFromRS;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User findUserById(UUID id) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_BY_ID_QUERY)) {
            ps.setObject(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String login = rs.getString(2);
                String password = rs.getString(3);
                String name = rs.getString(4);
                String surname = rs.getString(5);
                String city = rs.getString(6);
                int birthYear = rs.getInt(7);
                User userFromRS = new User(login, password, name, surname, city, birthYear);
                userFromRS.setId(id);
                return userFromRS;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateUser(User updateUser) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_USER_QUERY)) {
            ps.setString(1, updateUser.getLogin());
            ps.setString(2, updateUser.getPassword());
            ps.setString(3, updateUser.getName());
            ps.setString(4, updateUser.getSurname());
            ps.setString(5, updateUser.getCity());
            ps.setInt(6, updateUser.getBirthYear());
            ps.setObject(7, updateUser.getId());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

