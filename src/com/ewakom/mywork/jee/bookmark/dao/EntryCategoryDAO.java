package com.ewakom.mywork.jee.bookmark.dao;

import com.ewakom.mywork.jee.bookmark.db.DbConnector;
import com.ewakom.mywork.jee.bookmark.pojos.EntryCategory;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Stateless
public class EntryCategoryDAO {
    private static final String SELECT_ALL_QUERY = "SELECT entry_id, category_id from bookmark.public.entry_category";
    private static final String SELECT_BY_CATEGORY_ID_QUERY = "SELECT entry_id, category_id FROM bookmark.public.entry_category WHERE category_id = ?";
    private static final String INSERT_ENTRY_CATEGORY_QUERY = "INSERT INTO bookmark.public.entry_category VALUES (?,?)";
    private static final String UPDATE_ENTRY_CATEGORY_QUERY = "UPDATE bookmark.public.entry_category SET category_id = ? WHERE entry_id = ?";

    public List<EntryCategory> getAllEntryCategoryList() {
        List<EntryCategory> entryCategoryList = null;
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_QUERY)) {
            entryCategoryList = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String entryId = rs.getString(1);
                String categoryId = rs.getString(2);
                EntryCategory entryCategoryFromRs = new EntryCategory(UUID.fromString(entryId), UUID.fromString(categoryId));
                entryCategoryList.add(entryCategoryFromRs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entryCategoryList;
    }

    public List<EntryCategory> findEntryCategoryByCategoryId(UUID categoryId) {
        List<EntryCategory> entryCategoryList = null;
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_BY_CATEGORY_ID_QUERY)) {
            entryCategoryList = new ArrayList<>();
            ps.setObject(1, categoryId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String entryId = rs.getString(1);
                EntryCategory entryCategoryFromRs = new EntryCategory(UUID.fromString(entryId), categoryId);
                entryCategoryList.add(entryCategoryFromRs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entryCategoryList;
    }

    public void addEntryCategory(UUID entryId, UUID categoryId) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_ENTRY_CATEGORY_QUERY)) {
            ps.setObject(1, entryId);
            ps.setObject(2, categoryId);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void updateEntryCategory(UUID entryId, UUID categoryId) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_ENTRY_CATEGORY_QUERY)) {
            ps.setObject(1, entryId);
            ps.setObject(2, categoryId);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
