package com.ewakom.mywork.jee.bookmark.dao;

import com.ewakom.mywork.jee.bookmark.db.DbConnector;
import com.ewakom.mywork.jee.bookmark.pojos.Entry;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
public class EntryDAO {
    private static final String SELECT_ALL_QUERY = "SELECT id, description, deadline, done, created_at, priority, user_id, webaddress FROM bookmark.public.entry";
    private static final String SELECT_BY_ID_QUERY = "SELECT id, description, deadline, done, created_at, priority, user_id, webaddress FROM bookmark.public.entry WHERE id = ?";
    private static final String INSERT_ENTRY_QUERY = "INSERT INTO bookmark.public.entry VALUES (?,?,?,?,?,?,?,?)";
    private static final String DELETE_ENTRY_QUERY = "DELETE FROM bookmark.public.entry WHERE id = ?;";
    private static final String UPDATE_ENTRY_QUERY = "UPDATE bookmark.public.entry SET description = ?, webaddress = ?, deadline = ?, done = ?, created_at = ?, priority = ? WHERE id = ?";

    public List<Entry> getAllEntries() {
        List<Entry> entries = null;
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_QUERY)) {
            entries = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String description = rs.getString(2);
                Date daeadlineToConvert = rs.getDate(3);
                LocalDate deadline = new java.sql.Date(
                        daeadlineToConvert.getTime()).toLocalDate();
                Boolean done = rs.getBoolean(4);
                Date createDateToConvert = rs.getDate(5);
                LocalDate createDate = new java.sql.Date(
                        createDateToConvert.getTime()).toLocalDate();
                int priority = rs.getInt(6);
                String userId = rs.getString(7);
                String webaddress = rs.getString(8);
                Entry entryFromRS = new Entry(description, webaddress, deadline, done, createDate, priority, UUID.fromString(userId));
                entryFromRS.setId(UUID.fromString(id));
                entries.add(entryFromRS);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entries;
    }

    public Entry findEntryById(UUID id) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_BY_ID_QUERY)) {
            ps.setObject(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String description = rs.getString(2);
                Date daeadlineToConvert = rs.getDate(3);
                LocalDate deadline = new java.sql.Date(
                        daeadlineToConvert.getTime()).toLocalDate();
                boolean done = rs.getBoolean(4);
                Date createDateToConvert = rs.getDate(5);
                LocalDate createDate = new java.sql.Date(
                        createDateToConvert.getTime()).toLocalDate();
                int priority = rs.getInt(6);
                String userId = rs.getString(7);
                String webaddress = rs.getString(8);
                Entry entryFromRS = new Entry(description, webaddress, deadline, done, createDate, priority, UUID.fromString(userId));
                entryFromRS.setId(id);
                return entryFromRS;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addEntry(Entry entry) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_ENTRY_QUERY)) {
            ps.setObject(1, entry.getId());
            ps.setString(2, entry.getDescription());
            Date deadline = java.sql.Date.valueOf(entry.getDeadline());
            ps.setDate(3, (java.sql.Date) deadline);
            ps.setBoolean(4, entry.isDone());
            Date createDate = java.sql.Date.valueOf(entry.getCreateDate());
            ps.setDate(5, (java.sql.Date) createDate);
            ps.setInt(6, entry.getPriority());
            ps.setString(7, entry.getWebAddress());
            ps.setObject(8, entry.getUserId());

            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void removeEntry(UUID id) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_ENTRY_QUERY)) {
            ps.setObject(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void updateEntry(Entry entryToUpdate) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_ENTRY_QUERY)) {
            ps.setString(1, entryToUpdate.getDescription());
            ps.setString(2, entryToUpdate.getWebAddress());
            Date deadline = java.sql.Date.valueOf(entryToUpdate.getDeadline());
            ps.setDate(3, (java.sql.Date) deadline);
            ps.setBoolean(4, entryToUpdate.isDone());
            Date createDate = java.sql.Date.valueOf(entryToUpdate.getCreateDate());
            ps.setDate(5, (java.sql.Date) createDate);
            ps.setInt(6, entryToUpdate.getPriority());
            ps.setObject(7, entryToUpdate.getId());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
