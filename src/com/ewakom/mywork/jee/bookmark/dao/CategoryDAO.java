package com.ewakom.mywork.jee.bookmark.dao;

import com.ewakom.mywork.jee.bookmark.db.DbConnector;
import com.ewakom.mywork.jee.bookmark.pojos.Category;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Stateless
public class CategoryDAO {
    private static final String SELECT_ALL_QUERY = "SELECT id, name, user_id FROM bookmark.public.category";
    private static final String SELECT_BY_ID_QUERY = "SELECT id, name, user_id FROM bookmark.public.category WHERE id = ?";
    private static final String SELECT_BY_NAME_QUERY = "SELECT id, name, user_id FROM bookmark.public.category WHERE name = ?";
    private static final String INSERT_CATEGORY_QUERY = "INSERT INTO bookmark.public.category VALUES (?,?,?)";
    private static final String DELETE_CATEGORY_QUERY = "DELETE FROM bookmark.public.category WHERE id = ?;";
    private static final String UPDATE_CATEGORY_QUERY = "UPDATE bookmark.public.category SET name = ? WHERE id = ?";

    public List<Category> getAllCategories() {
        List<Category> categories = null;
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_QUERY)) {
            categories = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String userId = rs.getString(3);
                Category categoryFromRS = new Category(name, UUID.fromString(userId));
                categoryFromRS.setId(UUID.fromString(id));
                categories.add(categoryFromRS);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return categories;
    }

    public Category findCategoryById(UUID id) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_BY_ID_QUERY)) {
            ps.setObject(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String name = rs.getString(2);
                String userId = rs.getString(3);
                Category categoryFromRS = new Category(name, UUID.fromString(userId));
                categoryFromRS.setId(id);
                return categoryFromRS;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Category findCategoryByName(String name) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_BY_NAME_QUERY)) {
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String id = rs.getString(1);
                String userId = rs.getString(3);
                Category categoryFromRS = new Category(name, UUID.fromString(userId));
                categoryFromRS.setId(UUID.fromString(id));
                return categoryFromRS;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addCategory(Category category) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_CATEGORY_QUERY)) {
            ps.setObject(1, category.getId());
            ps.setString(2, category.getName());
            ps.setObject(3, category.getUserId());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void removeCategory(UUID id) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_CATEGORY_QUERY)) {
            ps.setObject(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void updateCategory(Category categoryToUpdate) {
        try (Connection conn = DbConnector.createConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_CATEGORY_QUERY)) {
            ps.setString(1, categoryToUpdate.getName());
            ps.setObject(2, categoryToUpdate.getId());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
