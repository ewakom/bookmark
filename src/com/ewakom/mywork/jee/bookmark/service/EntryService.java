package com.ewakom.mywork.jee.bookmark.service;

import com.ewakom.mywork.jee.bookmark.dao.EntryDAO;
import com.ewakom.mywork.jee.bookmark.pojos.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Stateless
public class EntryService {
    @EJB
    private EntryDAO entryDAO;

    public List<Entry> getAllEntries() {
        List<Entry> entryList = entryDAO.getAllEntries();
        entryList.sort((o1, o2) -> (o1.getUserId().compareTo(o2.getUserId())));
        return entryList;
    }

    public List<Entry> getAllEntriesForUser(UUID userId) throws MalformedURLException {
        List<Entry> entriesForUser = new ArrayList<>();
        for (Entry entry : entryDAO.getAllEntries()) {
            if (entry.getUserId().equals(userId)) {
                entriesForUser.add(entry);
            }
        }
        entriesForUser.sort(new Comparator<Entry>() {
            @Override
            public int compare(Entry o1, Entry o2) {
                return (o1.getDeadline()).compareTo(o2.getDeadline());
            }
        });
        return entriesForUser;
    }

    public Entry findEntryById(UUID id) throws IOException {
        return entryDAO.findEntryById(id);
    }

    public void addEntry(Entry entry) {
        entryDAO.addEntry(entry);
    }

    public void removeEntry(UUID id) {
        entryDAO.removeEntry(id);
    }

    public void updateEntry(Entry entryToUpdate) {
        entryDAO.updateEntry(entryToUpdate);
    }

}