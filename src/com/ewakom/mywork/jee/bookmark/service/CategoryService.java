package com.ewakom.mywork.jee.bookmark.service;

import com.ewakom.mywork.jee.bookmark.dao.CategoryDAO;
import com.ewakom.mywork.jee.bookmark.pojos.Category;
import com.ewakom.mywork.jee.bookmark.pojos.EntryCategory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Stateless
public class CategoryService {

    @EJB
    private CategoryDAO categoryDAO;
    @EJB
    private EntryCategoryService entryCategoryService;

    public List<Category> getAllCategories() {
        List<Category> categoryList = categoryDAO.getAllCategories();
        categoryList.sort((o1, o2) -> (o1.getUserId()).compareTo(o2.getUserId()));
        return categoryList;
    }

    public List<Category> getAllCategoriesForUser(UUID userId) {
        List<Category> categoriesForUser = new ArrayList<>();
        for (Category category : categoryDAO.getAllCategories()) {
            if (category.getUserId().equals(userId)) {
                categoriesForUser.add(category);
            }
        }
        categoriesForUser.sort((o1, o2) -> (o1.getName().compareTo(o2.getName())));
        return categoriesForUser;
    }

    public Category findCategoryById(UUID id) {
        return categoryDAO.findCategoryById(id);
    }

    public Category findCategoryByName(String name) {
        return categoryDAO.findCategoryByName(name);
    }


    public boolean addCategory(Category category) {
        Category existCategory = categoryDAO.findCategoryByName(category.getName());
        if (existCategory == null) {
            categoryDAO.addCategory(category);
            return true;
        }
        return false;
    }

    public boolean removeCategory(UUID id) {
        List<EntryCategory> entryCategoryList = entryCategoryService.getAllEntriesCategories();
        for (EntryCategory entryCategory : entryCategoryList) {
            if (entryCategory.getCategoryId().equals(id)) {
                if (entryCategory.getEntryId() != null) {
                    return false;
                } else {
                    categoryDAO.removeCategory(id);
                    return true;
                }
            }
        }
        categoryDAO.removeCategory(id);
        return true;
    }

    public boolean updateCategory(Category categoryToUpdate) {
        Category existCategory = categoryDAO.findCategoryByName(categoryToUpdate.getName());
        if (existCategory == null) {
            categoryDAO.updateCategory(categoryToUpdate);
            return true;
        }
        return false;
    }
}
