package com.ewakom.mywork.jee.bookmark.service;

import com.ewakom.mywork.jee.bookmark.dao.CategoryDAO;
import com.ewakom.mywork.jee.bookmark.dao.EntryCategoryDAO;
import com.ewakom.mywork.jee.bookmark.pojos.EntryCategory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import java.util.UUID;

@Stateless
public class EntryCategoryService {
    @EJB
    private EntryCategoryDAO entryCategoryDAO;
    @EJB
    private CategoryDAO categoryDAO;

    public void addEntryCategory(UUID entryId, UUID categoryId) {
        entryCategoryDAO.addEntryCategory(entryId, categoryId);
    }

    public void updateEntryCategory(UUID entryId, UUID categoryId) {
        entryCategoryDAO.updateEntryCategory(entryId, categoryId);
    }

    public List<EntryCategory> getAllEntriesCategories() {
        return entryCategoryDAO.getAllEntryCategoryList();
    }

    public List<EntryCategory> findEntryCategoryByCategoryId(UUID categoryId) {
        return entryCategoryDAO.findEntryCategoryByCategoryId(categoryId);
    }
}
