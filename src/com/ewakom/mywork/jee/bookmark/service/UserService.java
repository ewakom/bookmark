package com.ewakom.mywork.jee.bookmark.service;

import com.ewakom.mywork.jee.bookmark.dao.UserDAO;
import com.ewakom.mywork.jee.bookmark.pojos.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

@Stateless
public class UserService {
    @EJB
    private UserDAO userDAO;
    private static final String SALT = "a4e92j8fefr311-111";


    public List<User> getAllUsers() {
        List<User> userList = userDAO.getAllUsers();
        userList.sort((o1, o2) -> (o1.getLogin()).compareTo(o2.getLogin()));
        return userList;
    }

    public boolean registerUser(User user) {
        User existUser = userDAO.findUserByLogin(user.getLogin().toLowerCase());
        if (existUser == null) {
            userDAO.registerUser(user);
            return true;
        }
        return false;
    }

    public void removeUser(UUID id) {
        userDAO.removeUser(id);
    }

    public User findUserById(UUID id) {
        return userDAO.findUserById(id);
    }

    public User findUserByLogin(String login) {
        return userDAO.findUserByLogin(login);
    }

    public void updateUser(User updateUser) {
        userDAO.updateUser(updateUser);
    }

    public boolean updateUserLogin(User updateUser) {
        User existUserWithTheSameLogin = userDAO.findUserByLogin(updateUser.getLogin().toLowerCase());
        if (existUserWithTheSameLogin == null) {
            userDAO.updateUser(updateUser);
            return true;
        }
        return false;
    }

    public String hash(String passwordToHash) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(SALT.getBytes());
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
}
