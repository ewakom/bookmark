package com.ewakom.mywork.jee.bookmark.security;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

@WebFilter("/*")
public class LoginFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpSession session = req.getSession();
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        UUID userId = (UUID) session.getAttribute("userId");
        String requestURI = req.getRequestURI();
        if (userId != null || requestURI.endsWith("login.jsp") || requestURI.endsWith("login.do")
                || requestURI.endsWith("register.jsp") || requestURI.endsWith("register.do")
                || requestURI.endsWith("index.jsp")
                || requestURI.endsWith("error_login.jsp")
                || requestURI.endsWith(".gif")
                || requestURI.endsWith(".css")) {
            chain.doFilter(req, res);
        } else {
            res.sendRedirect(res.encodeRedirectURL(req.getContextPath() + "/index.jsp"));
        }
    }
}
