package com.ewakom.mywork.jee.bookmark.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {
    public static Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        String url = "jdbc:postgresql://localhost:5432/bookmark?user=postgres&password=postgres&ssl=false";
        return DriverManager.getConnection(url);
    }
}