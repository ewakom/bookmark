package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.User;
import com.ewakom.mywork.jee.bookmark.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login.do")
public class LoginServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        for (User user : userService.getAllUsers()) {
            String hashPassword = userService.hash(password);
            if (login.equals(user.getLogin()) && hashPassword.equals(user.getPassword())) {
                HttpSession session = req.getSession();
                session.setAttribute("userId", user.getId());
                session.setAttribute("login", user.getLogin());
                if (login.equals("admin")) {
                    req.getRequestDispatcher("/admin_page.jsp").forward(req, resp);
                } else {
                    req.getRequestDispatcher("/homepage.jsp").forward(req, resp);
                }
            }
        }
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }
}
