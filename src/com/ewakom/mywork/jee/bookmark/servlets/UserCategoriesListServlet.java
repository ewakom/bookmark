package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.Category;
import com.ewakom.mywork.jee.bookmark.service.CategoryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@WebServlet("/userCategories.do")
public class UserCategoriesListServlet extends HttpServlet {
    @EJB
    private CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        UUID userId = (UUID) session.getAttribute("userId");
        List<Category> categories = categoryService.getAllCategoriesForUser(userId);
        req.setAttribute("userCategories", categories);
        req.getRequestDispatcher("userCategories.jsp").forward(req, resp);
    }
}
