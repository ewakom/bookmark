package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.Category;
import com.ewakom.mywork.jee.bookmark.pojos.Entry;
import com.ewakom.mywork.jee.bookmark.pojos.EntryCategory;
import com.ewakom.mywork.jee.bookmark.service.CategoryService;
import com.ewakom.mywork.jee.bookmark.service.EntryCategoryService;
import com.ewakom.mywork.jee.bookmark.service.EntryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/entry_category.do")
public class EntryCategoryListServlet extends HttpServlet {

    @EJB
    private EntryCategoryService entryCategoryService;
    @EJB
    private EntryService entryService;
    @EJB
    private CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<EntryCategory> entryCategoryList = entryCategoryService.getAllEntriesCategories();
        req.setAttribute("entryCategoryList", entryCategoryList);
        List<Entry> entries = entryService.getAllEntries();
        req.setAttribute("entries", entries);
        List<Category> categories = categoryService.getAllCategories();
        req.setAttribute("categories", categories);
        req.getRequestDispatcher("entry_category_list.jsp").forward(req, resp);
    }
}
