package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.Category;
import com.ewakom.mywork.jee.bookmark.service.CategoryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/add-category.do")
public class AddCategoryServlet extends HttpServlet {

    @EJB
    private CategoryService categoryService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("name");
        HttpSession session = req.getSession();
        UUID userId = (UUID) session.getAttribute("userId");
        Category category = new Category(name, userId);
        boolean categoryAdded = categoryService.addCategory(category);
        if (categoryAdded) {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/userCategories.do"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/error_category_add.jsp"));
        }
    }
}
