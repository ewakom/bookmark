package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.service.CategoryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/delete-category.do")
public class DeleteCategoryServlet extends HttpServlet {
    @EJB
    private CategoryService categoryService;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        UUID uuid = UUID.fromString(id);
        boolean categoryDeleted = categoryService.removeCategory(uuid);
        if (categoryDeleted) {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/userCategories.do"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/error_category_delete.jsp"));
        }
    }
}
