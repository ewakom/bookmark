package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.User;
import com.ewakom.mywork.jee.bookmark.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/findUser.do")
public class FindUserServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        String login = req.getParameter("login");
        User editUser = userService.findUserByLogin(login);
        req.setAttribute("editUser", editUser);
        req.getRequestDispatcher("edit-user.jsp").forward(req, resp);
    }
}


