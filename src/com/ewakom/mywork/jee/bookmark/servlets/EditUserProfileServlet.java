package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.User;
import com.ewakom.mywork.jee.bookmark.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/edit-userProfile.do")
public class EditUserProfileServlet extends HttpServlet {

    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String login = req.getParameter("login");
        HttpSession session = req.getSession();
        UUID userId = (UUID) session.getAttribute("userId");
        User editUser = userService.findUserById(userId);
        req.setAttribute("editUser", editUser);
        req.getRequestDispatcher("edit-userProfile.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String newName = req.getParameter("name");
        String newSurname = req.getParameter("surname");
        String newCity = req.getParameter("city");
        String newBirthYear = req.getParameter("birthYear");
        String id = req.getParameter("id");
        User userToEdit = userService.findUserById(UUID.fromString(id));
        userToEdit.setName(newName);
        userToEdit.setSurname(newSurname);
        userToEdit.setCity(newCity);
        userToEdit.setBirthYear(Integer.valueOf(newBirthYear));
        userService.updateUser(userToEdit);
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/confirm_profile_changes.jsp"));
    }
}
