package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.Category;
import com.ewakom.mywork.jee.bookmark.pojos.Entry;
import com.ewakom.mywork.jee.bookmark.service.CategoryService;
import com.ewakom.mywork.jee.bookmark.service.EntryCategoryService;
import com.ewakom.mywork.jee.bookmark.service.EntryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@WebServlet("/add-entry.do")
public class AddEntryServlet extends HttpServlet {

    @EJB
    private EntryService entryService;
    @EJB
    private EntryCategoryService entryCategoryService;
    @EJB
    private CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        UUID userId = (UUID) session.getAttribute("userId");
        List<Category> categoryList = categoryService.getAllCategoriesForUser(userId);
        req.setAttribute("categoryList", categoryList);
        req.getRequestDispatcher("add-entry.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String description = req.getParameter("description");
        String webAddress = req.getParameter("webAddress");
        String createDate = req.getParameter("createdate");
        String deadline = req.getParameter("deadline");
        String priority = req.getParameter("priority");
        String done = req.getParameter("done");
        String categoryId = req.getParameter("categoryId");
        HttpSession session = req.getSession();
        UUID userId = (UUID) session.getAttribute("userId");
        Entry entry = new Entry(description, webAddress, LocalDate.parse(deadline), Boolean.valueOf(done), LocalDate.parse(createDate),
                Integer.valueOf(priority), userId);
        entryService.addEntry(entry);
        entryCategoryService.addEntryCategory(entry.getId(), UUID.fromString(categoryId));
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/userEntries.do"));

    }
}
