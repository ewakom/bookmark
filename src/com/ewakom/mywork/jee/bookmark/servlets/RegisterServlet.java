package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.User;
import com.ewakom.mywork.jee.bookmark.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register.do")
public class RegisterServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String city = req.getParameter("city");
        String birthYear = req.getParameter("birthYear");
        User user = new User(login, userService.hash(password), name, surname, city, Integer.valueOf(birthYear));
        boolean registerUserSuccess = userService.registerUser(user);
        if (registerUserSuccess) {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/login.jsp"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/error_login.jsp"));
        }
    }
}
