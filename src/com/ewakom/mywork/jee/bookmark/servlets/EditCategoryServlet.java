package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.Category;
import com.ewakom.mywork.jee.bookmark.service.CategoryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/edit-category.do")
public class EditCategoryServlet extends HttpServlet {

    @EJB
    private CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String id = req.getParameter("id");
        Category categoryToEdit = categoryService.findCategoryById(UUID.fromString(id));
        req.setAttribute("categoryToEdit", categoryToEdit);
        req.getRequestDispatcher("edit-category.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String newName = req.getParameter("name");
        String id = req.getParameter("id");
        Category categoryToEdit = categoryService.findCategoryById(UUID.fromString(id));
        categoryToEdit.setName(newName);
        boolean categoryUpdated = categoryService.updateCategory(categoryToEdit);
        if (categoryUpdated) {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/userCategories.do"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/error_category_add.jsp"));
        }
    }
}
