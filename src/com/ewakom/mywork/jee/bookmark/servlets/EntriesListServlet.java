package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.Entry;
import com.ewakom.mywork.jee.bookmark.service.EntryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/entries.do")
public class EntriesListServlet extends HttpServlet {

    @EJB
    private EntryService entryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<Entry> entries = entryService.getAllEntries();
        req.setAttribute("entries", entries);
        req.getRequestDispatcher("entries.jsp").forward(req, resp);
    }
}
