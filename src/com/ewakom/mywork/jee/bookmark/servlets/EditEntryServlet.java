package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.Category;
import com.ewakom.mywork.jee.bookmark.pojos.Entry;
import com.ewakom.mywork.jee.bookmark.service.CategoryService;
import com.ewakom.mywork.jee.bookmark.service.EntryCategoryService;
import com.ewakom.mywork.jee.bookmark.service.EntryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@WebServlet("/edit-entry.do")
public class EditEntryServlet extends HttpServlet {
    @EJB
    private EntryService entryService;
    @EJB
    private CategoryService categoryService;
    @EJB
    private EntryCategoryService entryCategoryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String id = req.getParameter("id");
        Entry entryToEdit = entryService.findEntryById(UUID.fromString(id));
        req.setAttribute("entryToEdit", entryToEdit);
        HttpSession session = req.getSession();
        UUID userId = (UUID) session.getAttribute("userId");
        List<Category> categoryList = categoryService.getAllCategoriesForUser(userId);
        req.setAttribute("categoryList", categoryList);
        req.getRequestDispatcher("edit-entry.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String newDescription = req.getParameter("description");
        String newWebAddress = req.getParameter("webAddress");
        String newCreateDate = req.getParameter("createdate");
        String newDeadline = req.getParameter("deadline");
        String newPriority = req.getParameter("priority");
        String newDone = req.getParameter("done");
        String id = req.getParameter("id");
        Entry entryToEdit = entryService.findEntryById(UUID.fromString(id));
        entryToEdit.setDescription(newDescription);
        entryToEdit.setWebAddress(newWebAddress);
        entryToEdit.setCreateDate(LocalDate.parse(newCreateDate));
        entryToEdit.setDeadline(LocalDate.parse(newDeadline));
        entryToEdit.setPriority(Integer.valueOf(newPriority));
        entryToEdit.setDone(Boolean.valueOf(newDone));
        entryService.updateEntry(entryToEdit);
        String categoryId = req.getParameter("categoryId");
        entryCategoryService.updateEntryCategory(entryToEdit.getId(), UUID.fromString(categoryId));
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/userEntries.do"));
    }
}
