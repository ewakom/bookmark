package com.ewakom.mywork.jee.bookmark.servlets;

import com.ewakom.mywork.jee.bookmark.pojos.User;
import com.ewakom.mywork.jee.bookmark.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/edit-userPassword.do")
public class EditUserPasswordServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String newPassword = req.getParameter("newPassword");
        String repeatNewPassword = req.getParameter("repeatNewPassword");
        String id = req.getParameter("id");
        User userToEdit = userService.findUserById(UUID.fromString(id));
        if (newPassword.equals(repeatNewPassword)) {
            userToEdit.setPassword(userService.hash(newPassword));
            userService.updateUser(userToEdit);
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/confirm_profile_changes.jsp"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/edit-userProfile.do"));
        }
    }
}

