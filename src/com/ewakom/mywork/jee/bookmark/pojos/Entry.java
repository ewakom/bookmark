package com.ewakom.mywork.jee.bookmark.pojos;

import java.time.LocalDate;
import java.util.UUID;

public class Entry {
    private UUID id;
    private String description;
    private String webAddress;
    private LocalDate deadline;
    private boolean done;
    private LocalDate createDate;
    private int priority;
    private UUID userId;

    public Entry(String description, String webAddress, LocalDate deadline, boolean done, LocalDate createDate, int priority, UUID userId) {
        this.id = UUID.randomUUID();
        this.description = description;
        this.webAddress = webAddress;
        this.deadline = deadline;
        this.done = done;
        this.createDate = createDate;
        this.priority = priority;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", webAdress=" + webAddress +
                ", deadline=" + deadline +
                ", done=" + done +
                ", createDate=" + createDate +
                ", priority=" + priority +
                ", userId=" + userId +
                '}';
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createdDate) {
        this.createDate = createdDate;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }
}
