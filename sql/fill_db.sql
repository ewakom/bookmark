insert into jee_user values('d9e7ab9e-76ca-4967-bcc4-4300f80d7e53','admin', '95e0b946cc86edb65759c301f10a4a0f86300367bad9b5a792ea7bfb9da97e417d76c5bb13b49f4aebd592cc98fe4a07bea3fd2a5bf4eb868203976b9bc23c1f', 'admin','admin','admin',2000); /*password = admin*/
insert into jee_user values('a775ed0f-f4e9-4162-92bf-fd1d7c0756ed','anowak', '317af4b7ffb75be953ea53330de548c84126dccb2129747ec44ebcb327c99fe9935692ae03880d1b0bfa52f64abf31cc29e08595da53fd32bd97c881609dbfee', 'adam','nowak','Wrocław',1994); /*password = anowak*/
insert into jee_user values('e12668f4-745c-4d87-847e-a3e798f92734','apolak', '2e63283dcc9efb66ed14b5f744f74dea8021f51070294a9cc3b30607431fdd4ed6fdc63e0254fd5b4b3ae14bc3c0fe38d355e5f0cb6373a78869b2d4c5bac41e', 'anna','polak','Warszawa',2001); /*password = apolak*/
insert into jee_user values('38332a04-740f-4044-93ef-41859273b8b5','jkowalski', '05237cf4e9656a737bbe9c0d382e287bb6d7c9052d2f75544b06808950e4e070b51854033b42c0709244c4521c81c233de73e462fa129fa9a45033a2dd99c49a', 'jan','kowalski','Kraków',1978); /*password = jkowalski*/

insert into entry values ('63e6e827-83a7-41c1-8f8a-2d69941cc819', 'newest Robin Cook book', '2019-12-31', false, '2019-09-16','1','https://www.empik.com/pandemia-cook-robin,p1228461183,ebooki-i-mp3-p','a775ed0f-f4e9-4162-92bf-fd1d7c0756ed');
insert into entry values ('29a4dec8-bd19-4f97-ad25-9168de9db46c', 'JS tutorial', '2019-12-31', false, '2019-09-16', '4','https://www.youtube.com/watch?v=PkZNo7MFNFg','a775ed0f-f4e9-4162-92bf-fd1d7c0756ed');
insert into entry values ('89c900e3-5646-4f71-ae31-312f57755df4', 'JAVA courses', '2020-05-31', false, '2019-09-16', '1','https://www.udemy.com/topic/java/','a775ed0f-f4e9-4162-92bf-fd1d7c0756ed');
insert into entry values ('06f336ff-91c1-49f8-baba-9fd8a5d36a73', 'about raising children ', '2019-08-31', true, '2019-01-16', '4','https://www.empik.com/jak-mowic-zeby-dzieci-nas-sluchaly-faber-adele-mazlish-elaine,p1055071104,ksiazka-p','a775ed0f-f4e9-4162-92bf-fd1d7c0756ed');
insert into entry values ('800bf6f0-fd56-4973-a332-d5f857dc24f8', 'JAVA courses', '2021-12-31', false, '2019-09-16', '2','https://www.udemy.com/topic/java/','e12668f4-745c-4d87-847e-a3e798f92734');
insert into entry values ('83bdbb21-3907-4e70-bd45-9fd49fc13bfe', 'Blog dla kobiet', '2019-11-30', false, '2019-09-16', '5','http://mumme.pl/?gclid=CjwKCAjw5fzrBRASEiwAD2OSV4bocQcLYhrGN2R8wPqQVBwIK0mjjqdDgsomyiim4dGvhewzJ9_ySRoCePkQAvD_BwE','e12668f4-745c-4d87-847e-a3e798f92734');
insert into entry values ('416464b2-6509-4577-83e2-994f93929c28', 'wychowanie dzieci okiem ojca', '2020-12-31', false, '2019-09-16', '6','https://www.blogojciec.pl/','e12668f4-745c-4d87-847e-a3e798f92734');
insert into entry values ('dbd628e5-1e30-4423-99e1-dfa301b74f2e', 'Docker Tutorial - What is Docker & Docker Containers, Images, etc?', '2019-12-31', false, '2019-09-16', '2','https://www.youtube.com/watch?v=pGYAg7TMmp0','38332a04-740f-4044-93ef-41859273b8b5');
insert into entry values ('e7ac81f2-76a9-4f6e-b811-82048821cff4', 'Learn Docker in 12 Minutes', '2019-12-31', false, '2019-09-16', '2','https://www.youtube.com/watch?v=YFl2mCHdv24','38332a04-740f-4044-93ef-41859273b8b5');
insert into entry values ('554ac2ad-8f94-44c2-bedf-cb2991f104e7', 'Docker. Projektowanie i wdrażanie aplikacji', '2019-10-31', false, '2019-09-16', '1','https://www.empik.com/docker-projektowanie-i-wdrazanie-aplikacji-krochmalski-jaroslaw,p1158314636,ksiazka-p','38332a04-740f-4044-93ef-41859273b8b5');

insert into category values ('86618069-01af-4cff-ab2d-9e9c53eccb3a','book to buy','a775ed0f-f4e9-4162-92bf-fd1d7c0756ed');
insert into category values ('e8493085-3ff6-4947-9c04-e4d8c2f54cd6','tutorial','a775ed0f-f4e9-4162-92bf-fd1d7c0756ed');
insert into category values ('718dafe4-65b2-4cfe-b0ad-6c8944e4545f','course','a775ed0f-f4e9-4162-92bf-fd1d7c0756ed');
insert into category values ('a9f56e50-40f7-4f61-8ae2-e642eb8ac257','blog','e12668f4-745c-4d87-847e-a3e798f92734');
insert into category values ('f4f78934-bcc1-471d-b94b-96e779a0901e','course','e12668f4-745c-4d87-847e-a3e798f92734');
insert into category values ('c340b40a-dfdb-4731-a1a8-6fb87956e2f3','tutorial','38332a04-740f-4044-93ef-41859273b8b5');
insert into category values ('479e8485-50fb-48ba-8b7f-18dfa986bd3a','book to buy','38332a04-740f-4044-93ef-41859273b8b5');


insert into entry_category values ('63e6e827-83a7-41c1-8f8a-2d69941cc819','86618069-01af-4cff-ab2d-9e9c53eccb3a');
insert into entry_category values ('29a4dec8-bd19-4f97-ad25-9168de9db46c','e8493085-3ff6-4947-9c04-e4d8c2f54cd6');
insert into entry_category values ('89c900e3-5646-4f71-ae31-312f57755df4','718dafe4-65b2-4cfe-b0ad-6c8944e4545f');
insert into entry_category values ('06f336ff-91c1-49f8-baba-9fd8a5d36a73','86618069-01af-4cff-ab2d-9e9c53eccb3a');
insert into entry_category values ('800bf6f0-fd56-4973-a332-d5f857dc24f8','f4f78934-bcc1-471d-b94b-96e779a0901e');
insert into entry_category values ('83bdbb21-3907-4e70-bd45-9fd49fc13bfe','a9f56e50-40f7-4f61-8ae2-e642eb8ac257');
insert into entry_category values ('416464b2-6509-4577-83e2-994f93929c28','a9f56e50-40f7-4f61-8ae2-e642eb8ac257');
insert into entry_category values ('dbd628e5-1e30-4423-99e1-dfa301b74f2e','c340b40a-dfdb-4731-a1a8-6fb87956e2f3');
insert into entry_category values ('e7ac81f2-76a9-4f6e-b811-82048821cff4','c340b40a-dfdb-4731-a1a8-6fb87956e2f3');
insert into entry_category values ('554ac2ad-8f94-44c2-bedf-cb2991f104e7','479e8485-50fb-48ba-8b7f-18dfa986bd3a');
