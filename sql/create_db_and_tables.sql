create database bookmark;
create table jee_user (
                          id uuid primary key,
                          login varchar(255) not null,
                          password varchar(255) not null,
                          name varchar(255),
                          surname varchar(255),
                          city varchar(255),
                          birth_year INT
);

create table category (
                          id uuid primary key,
                          name varchar(255),
                          user_id uuid references jee_user(id)
);

create table entry (
                       id uuid primary key,
                       description varchar(255),
                       deadline date,
                       done boolean,
                       created_at date,
                       priority int,
                       webaddress varchar(255),
                       user_id uuid references jee_user(id)
);

create table entry_category (
                                entry_id uuid references entry(id),
                                category_id uuid references category(id),
                                primary key (entry_id, category_id)
);
