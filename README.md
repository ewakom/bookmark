### "Bookmark" - JAVA EE Web Application
Bookmark is a web application which was written for the practical testing of JEE code.
I wrote this App because I don't like bookmarks in browsers and I want to have it better organized.
###
The application development environment:
- GlassFish server.
- PostgreSQL database.
- IntelliJ IDEA
### About this application:
- login (session), logout and register mechanisms
- application security filter
- Bootstrap
- database connection
- EJB, dependency injection, Model-View-Controller
- available options: add, edit, delete (User, Entry, Category)
- Salted Password Hashing (SHA-512)
