<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Category" %>
<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Entry" %>
<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.EntryCategory" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.UUID" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - User Entries</title>

</head>
<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-11 mx-auto">
                <div style="margin: auto; color: darkgreen">
                    <h1>ENTRY List for
                        <%
                            List<Category> categories = (List<Category>) request.getAttribute("categories");
                            UUID categoryId = (UUID) request.getAttribute("categoryId");
                            for (Category category : categories) {
                                if (categoryId.equals(category.getId())) {
                                    out.write(category.getName());
                                }
                            }
                        %>
                    </h1>
                    <br/>
                </div>
                <div class="table-responsive-lg">
                    <table class="table table-striped table-dark overflow-hidden">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">description</th>
                            <th scope="col">web address</th>
                            <th scope="col">deadline</th>
                            <th scope="col">done</th>
                            <th scope="col">createDate</th>
                            <th scope="col">priority</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            List<EntryCategory> entryCategoryList = (List<EntryCategory>) request.getAttribute("entryCategoryList");
                            List<Entry> entries = (List<Entry>) request.getAttribute("entries");
                            int i = 0;
                            for (EntryCategory entryCategory : entryCategoryList) {
                                i++;
                        %>
                        <tr>
                            <%
                                UUID entryId = entryCategory.getEntryId();
                                for (Entry entry : entries) {
                                    if (entryId.equals(entry.getId())) {
                            %>
                            <td scope="row"><%=i%>
                            </td>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;"><%=entry.getDescription()%>
                            </td>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">
                                <a href="<%=entry.getWebAddress()%>"><%=entry.getWebAddress()%>
                                </a>
                            </td>
                            <td><%=entry.getDeadline()%>
                            </td>
                            <td><%=entry.isDone()%>
                            </td>
                            <td><%=entry.getCreateDate()%>
                            </td>
                            <td><%=entry.getPriority()%>
                            </td>
                            <%
                                        }
                                    }
                                }
                            %>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
