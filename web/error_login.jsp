<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - EDIT USER PROFILE</title>

</head>

<body>
<%
    if (session.getAttribute("login") == null) {
%>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.jsp">Bookmark - start page</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>
<%
} else if (session.getAttribute("login").equals("admin")) {
%>
<%@include file="layout/admin_header.jsp" %>
<% } else {
%>
<%@include file="layout/header.jsp" %>
<%
    }
%>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mx-auto">

                <div class="card-deck">
                    <div class="card">
                        <div class="card-header">
                            ERROR
                        </div>
                        <div class="card-body">
                            <br/>
                            <p>User with this login already exists ! </p>
                            <br/>
                            <p>Try again ! </p>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
