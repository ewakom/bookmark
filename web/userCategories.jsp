<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Category" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - User Categories</title>

</head>
<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 mx-auto">
                <div style="margin: auto; color: darkgreen">
                    <h1>CATEGORIES</h1>
                    <br/>
                </div>
                <div class="table-responsive-lg">
                    <table class="table table-striped table-dark overflow-hidden">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">name</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            List<Category> categoriesList = (List<Category>) request.getAttribute("userCategories");
                            int i = 0;
                            for (Category category : categoriesList) {
                                i++;
                        %>
                        <tr>
                            <th scope="row"><%=i%>
                            </th>
                            <td><%=category.getName()%>
                            </td>
                            <td>
                                <a
                                        class="btn btn-danger"
                                        href="delete-category.do?id=<%=category.getId()%>">DELETE</a>
                                <a
                                        class="btn btn-info"
                                        href="edit-category.do?id=<%=category.getId()%>">EDIT</a>
                                <a
                                        class="btn btn-info"
                                        href="user_entry_category.do?id=<%=category.getId()%>">Show Entries</a>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                    <a href="add-category.jsp" class="btn btn-danger">ADD</a>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
