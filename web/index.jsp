<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>

    <title>JEE APP - BOOKMARK</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="css/scrolling-nav.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Bookmark</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.jsp">ADMIN</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<header class="bg-info text-white">
    <div class="container text-center">
        <h1>Welcome to Bookmark</h1>
        <br/>
        <img src="images/bookmark.gif" width="200" height="200" class="mr-3" width="200" height="150" alt="image 1">
    </div>
</header>

<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>About this aplication</h2>
                <p class="lead">
                    Bookmark is a small web application which was written for the practical testing of JEE code.
                    If you don't like bookmarks in browsers and want to have it better organized , then this App is
                    your solution.
                </p>
                <ul>
                    <li>Categories - you can create your own and edit them.</li>
                    <br/>
                    <li>Entries - you can add, edit and assign to the created category.<br>
                        You can set deadline, priority and add web address.
                    </li>
                    <br/>
                    <li>Your Profile - you can edit your data at any time.</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="login" class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>Login</h2>
                <p class="lead">If you have an account login to the App</p>
                <a href="login.jsp"
                   class="btn btn-outline-info mt-3">LOGIN</a>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>Register</h2>
                <p class="lead">If you don't have an account you must to register first.</p>
                <a href="register.jsp"
                   class="btn btn-outline-info mt-3">REGISTER</a>
            </div>
        </div>
    </div>

</section>
<!-- Footer -->

<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ewa Komorowska 2019</p>
    </div>
</footer>

</body>
</html>
