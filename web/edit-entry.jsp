<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Category" %>
<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Entry" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - EDIT ENTRY</title>

</head>

<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="card text-center">
                    <div class="card-header">
                        EDIT DATA
                    </div>
                    <%
                        Entry entry = (Entry) request.getAttribute("entryToEdit");
                    %>
                    <form action="edit-entry.do" method="post" accept-charset="UTF-8">
                        <div class="form-group">
                            <label for="description" class="mt-2">DESCRIPTION</label>
                            <textarea rows="3" cols="20" class="form-control" name="description"
                                      id="description" required><%=entry.getDescription()%></textarea>
                        </div>
                        <div class="form-group">
                            <label for="webAddress" class="mt-2">WEB ADDRESS</label>

                            <input type="url" class="form-control" name="webAddress"
                                   id="webAddress" value="<%=entry.getWebAddress()%>" required pattern="https?://.+"
                                   title="Include http://">
                        </div>
                        <div class="form-group">
                            <label for="categoryId">CATEGORY</label>
                            </br>
                            <select type="text" class="form-control" name="categoryId" id="categoryId">
                                <%
                                    List<Category> categories = (List<Category>) request.getAttribute("categoryList");

                                    for (Category category : categories) {
                                %>
                                <option value="<%=category.getId()%>"><%=category.getName()%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="createdate">CREATE DATE</label>
                                <input type="date" class="form-control" value="<%=entry.getCreateDate()%>"
                                       name="createdate"
                                       id="createdate" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="deadline">DEADLINE</label>
                                <input type="date" class="form-control" value="<%=entry.getDeadline()%>" name="deadline"
                                       id="deadline" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="priority">PRIORITY</label>
                                <br/>
                                <select class="form-control" name="priority" id="priority">
                                    <option selected="selected">
                                        <%=entry.getPriority()%>
                                    </option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                    <option value=6>6</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="done">DONE</label>
                                <br/>
                                <select class="form-control" name="done" id="done">
                                    <option>
                                        <%
                                            String booleanValue;
                                            if (entry.isDone()) {
                                                booleanValue = "YES";
                                            } else {
                                                booleanValue = "NO";
                                            }
                                        %>
                                        <%=booleanValue%>
                                    </option>
                                    <option value="true">YES</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control" value="<%=entry.getId()%>"
                                   name="id" id="id">
                        </div>
                        <button type="submit" class="btn btn-primary">EDIT</button>
                        <br/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
