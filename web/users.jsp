<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - User List</title>

</head>
<body>
<%@include file="layout/admin_header.jsp" %>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div style="margin: auto; color: darkgreen">
                    <h1>USERS</h1>
                    <br/>
                </div>
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">User ID</th>
                        <th scope="col">Login</th>
                        <th scope="col">Name</th>
                        <th scope="col">Surname</th>
                        <th scope="col">Birth Year</th>
                        <th scope="col">City</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        List<User> users = (List<User>) request.getAttribute("users");
                        int i = 0;
                        for (User user : users) {
                            i++;
                    %>
                    <tr>
                        <td><%=user.getId()%>
                        </td>
                        <td><%=user.getLogin()%>
                        </td>
                        <td><%=user.getName()%>
                        </td>
                        <td><%=user.getSurname()%>
                        </td>
                        <td><%=user.getBirthYear()%>
                        </td>
                        <td><%=user.getCity()%>
                        </td>
                        <td>
                            <a
                                    class="btn btn-danger"
                                    href="delete.do?id=<%=user.getId()%>">DELETE</a>
                            <a
                                    class="btn btn-info"
                                    href="findUser.do?login=<%=user.getLogin()%>">EDIT</a>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    </tbody>
                </table>
                <a href="add-user.jsp" class="btn btn-danger">ADD</a>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
