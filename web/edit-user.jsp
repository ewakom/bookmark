<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - EDIT USER PROFILE</title>

</head>
<body>
<%
    if (session.getAttribute("login").equals("admin")) {
%>
<%@include file="layout/admin_header.jsp" %>
<% } else {
%>
<%@include file="layout/header.jsp" %>
<%
    }
%>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mx-auto">

                <div class="card-deck">
                    <div class="card">
                        <div class="card-header">
                            EDIT DATA
                        </div>
                        <div class="card-body">
                            <%
                                User user = (User) request.getAttribute("editUser");
                            %>
                            <form action="edit-userProfile.do" method="post" accept-charset="UTF-8">
                                <%request.setCharacterEncoding("UTF-8");%>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">NAME</label>
                                        <input type="text" class="form-control" value="<%=user.getName()%>"
                                               name="name" id="name" required pattern="[a-zA-Z]{2,}"
                                               title="only letters, at least 2">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="surname">SURNAME</label>
                                        <input type="text" class="form-control" value="<%=user.getSurname()%>"
                                               name="surname" id="surname" required pattern="[a-zA-Z]{2,}"
                                               title="only letters, at least 2">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="city">CITY</label>
                                        <input type="text" class="form-control" value="<%=user.getCity()%>"
                                               name="city" id="city" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="birthYear">BIRTH YEAR</label>
                                        <input type="number" min="1900" max="2019" class="form-control"
                                               value=<%=user.getBirthYear()%>
                                                       name="birthYear" id="birthYear" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" value="<%=user.getId()%>"
                                           name="id" id="id">
                                </div>
                                <button type="submit" class="btn btn-primary">EDIT</button>
                            </form>
                            <form action="edit-userLogin.do" method="post" accept-charset="UTF-8">
                                <%request.setCharacterEncoding("UTF-8");%>
                                <div class="form-group">
                                    <label for="login">LOGIN</label>
                                    <input type="text" class="form-control" value="<%=user.getLogin()%>"
                                           name="login" id="login" required pattern="[a-zA-Z]{3,}"
                                           title="only letters, at least 3">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" value="<%=user.getId()%>"
                                           name="id">
                                </div>
                                <button type="submit" class="btn btn-primary">EDIT LOGIN</button>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            EDIT PASSWORD
                        </div>
                        <div class="card-body">
                            <form action="edit-userPassword.do" method="post" accept-charset="UTF-8">
                                <%request.setCharacterEncoding("UTF-8");%>
                                <div class="form-group">
                                    <label for="myInput" class="mt-2">NEW PASSWORD</label>
                                    <input type="password" class="form-control"
                                           name="newPassword" id="myInput" required>
                                </div>
                                <div class="form-group">
                                    <label for="repeatNewPassword" class="mt-2">REPEAT NEW PASSWORD</label>
                                    <input type="password" class="form-control"
                                           name="repeatNewPassword" id="repeatNewPassword" required>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" value="<%=user.getId()%>"
                                           name="id">
                                </div>
                                <button type="submit" class="btn btn-primary">EDIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
