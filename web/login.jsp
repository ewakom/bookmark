<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>

    <title>BOOKMARK - LOGIN</title>

</head>

<body>

<%@include file="layout/login_register_header.jsp" %>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mx-auto">
                <div class="card text-center">
                    <div class="card-header">
                        LOGIN
                    </div>
                    <div class="card-body">
                        <form action="login.do" method="post">
                            <label for="login">Login</label>
                            <input type="text" class="form-control" name="login" id="login">
                            <br/>
                            <label for="password" class="mt-2">Password</label>
                            <input type="password" class="form-control" name="password" id="password">
                            <br/>
                            <button type="submit" class="btn btn-primary">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
