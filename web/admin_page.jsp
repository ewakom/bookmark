<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - ADMIN PAGE</title>

</head>

<body>

<%@include file="layout/admin_header.jsp" %>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="card-group">
                    <div class="card">
                        <img src="images/entries.jpg" width="200" height="195" class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title">Entries</h5>
                            <a class="btn btn-outline-info mt-3" href="entries.do">Show</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="images/categories.jpg" width="200" height="195" class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title">Categories</h5>
                            <a class="btn btn-outline-info mt-3" href="categories.do">Show</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="images/entry_category.jpg" width="200" height="195" class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title">Entry-Category List</h5>
                            <a class="btn btn-outline-info mt-3" href="entry_category.do">Show</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="images/users.jpg" width="200" height="195" class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title">Users</h5>
                            <a class="btn btn-outline-info mt-3" href="users.do">Show</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
