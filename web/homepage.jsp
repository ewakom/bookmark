<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>

    <title>BOOKMARK - HOME PAGE</title>

</head>
<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="card-group">
                    <div class="card">
                        <img src="images/entries.jpg" width="200" height="195" class="card-img-top" alt="entries">
                        <div class="card-body">
                            <h5 class="card-title">Your Entries</h5>
                            <p class="card-text">add - edit - delete</p>
                            <br/>
                            <a class="btn btn-outline-info mt-3" href="userEntries.do">Entries</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="images/categories.jpg" width="200" height="195" class="card-img-top" alt="categories">
                        <div class="card-body">
                            <h5 class="card-title">Your Categories</h5>
                            <p class="card-text">add - edit - delete <br> show entries</p>
                            <a class="btn btn-outline-info mt-3" href="userCategories.do">Categories</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="images/users.jpg" width="200" height="195" class="card-img-top" alt="users">
                        <div class="card-body">
                            <h5 class="card-title">Your Profile</h5>
                            <p class="card-text">edit: data - login - password</p>
                            <br/>
                            <a class="btn btn-outline-info mt-3" href="edit-userProfile.do">Yuor Profile</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
