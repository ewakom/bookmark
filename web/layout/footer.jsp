    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <html>

        <head>

        <title>Scrolling Nav - Start Bootstrap Template</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link href="css/scrolling-nav.css" rel="stylesheet">

        </head>

        <body id="page-top">

        <!-- Footer -->

        <footer class="py-5 bg-dark">
        <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ewa Komorowska 2019</p>
        </div>
        </footer>
        </body>
        </html>
