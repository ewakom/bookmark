<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - EDIT USER PROFILE</title>

</head>
<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-header">
                            ERROR
                        </div>
                        <div class="card-body">
                            <p>You cannot delete a category ! </p>
                            <p>An entry is assigned to this category. </p>
                            <p>Delete the entry or assign the entry to another category and then delete this
                                category. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
