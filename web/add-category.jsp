<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - ADD CATEGORY</title>

</head>

<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 mx-auto">
                <div class="card text-center">
                    <div class="card-header">
                        ADD CATEGORY
                    </div>
                    <form action="add-category.do" method="post" accept-charset="UTF-8">
                        <div class="form-group">
                            <br>
                            <br>
                            <label for="name">CATEGORY NAME</label>
                            <br>
                            <br>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">ADD</button>
                        <br>
                        <br>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
