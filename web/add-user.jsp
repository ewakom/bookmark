<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - ADD USER</title>

</head>

<body>

<%@include file="layout/admin_header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="card text-center">
                    <div class="card-header">
                        ADD USER
                    </div>
                    <form action="add-user.do" method="post" accept-charset="UTF-8">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">NAME</label>
                                <input type="text" class="form-control" name="name" id="name" required
                                       pattern="[a-zA-Z]{2,}" title="only letters, at least 2">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="surname">SURNAME</label>
                                <input type="text" class="form-control" name="surname" id="surname" required
                                       pattern="[a-zA-Z]{2,}" title="only letters, at least 2">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="city">CITY</label>
                                <input type="text" class="form-control" name="city" id="city" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="birthYear">BIRTH YEAR</label>
                                <input type="number" min="1900" max="2019" class="form-control" name="birthYear"
                                       id="birthYear" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="login">LOGIN</label>
                            <input type="text" class="form-control" name="login" id="login" required
                                   pattern="[a-zA-Z]{3,}" title="only letters, at least 3">
                        </div>
                        <div class="form-group">
                            <label for="password" class="mt-2">PASSWORD</label>
                            <input type="password" class="form-control" name="password" id="password" required>
                        </div>
                        <button type="submit" class="btn btn-primary">ADD</button>
                        <br/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
