<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Category" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>

    <title>BOOKMARK - EDIT CATEGORY</title>

</head>

<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mx-auto">

                <div class="card text-center">
                    <div class="card-header">
                        EDIT DATA
                    </div>
                    <br/>
                    <%
                        Category category = (Category) request.getAttribute("categoryToEdit");
                    %>
                    <form action="edit-category.do" method="post" accept-charset="UTF-8">
                        <%request.setCharacterEncoding("UTF-8");%>
                        <div class="form-group">
                            <label for="name">CATEGORY NAME</label>
                            <input type="text" class="form-control" value="<%=category.getName()%>" name="name"
                                   id="name" required>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control" value="<%=category.getId()%>"
                                   name="id" id="id">
                        </div>
                        <br/>
                        <button type="submit" class="btn btn-primary">EDIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
