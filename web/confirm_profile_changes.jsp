<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>

    <title>BOOKMARK - EDIT USER PROFILE</title>

</head>

<body>

<%
    if (session.getAttribute("login").equals("admin")) {
%>
<%@include file="layout/admin_header.jsp" %>
<% } else {
%>
<%@include file="layout/header.jsp" %>
<%
    }
%>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 mx-auto">

                <div class="card-deck">
                    <div class="card">
                        <div class="card-header">
                            CONFIRM CHANGES
                        </div>
                        <div class="card-body">
                            <%
                                if (session.getAttribute("login").equals("admin")) {
                            %>
                            <p>Data has been changed ! </p>
                            <% } else {
                            %>
                            <p>Your profile data has been changed ! </p>
                            <%
                                }
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
