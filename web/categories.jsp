<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Category" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - User Categories</title>

</head>

<body>

<%@include file="layout/admin_header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div style="margin: auto; color: darkgreen">
                    <h1>CATEGORIES</h1>
                    <br/>
                </div>
                <div class="table-responsive-lg">
                    <table class="table table-striped table-dark overflow-hidden">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">User ID</th>
                            <th scope="col">Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            List<Category> categoriesList = (List<Category>) request.getAttribute("categories");
                            int i = 0;
                            for (Category category : categoriesList) {
                                i++;
                        %>
                        <tr>
                            <td scope="row"><%=i%>
                            </td>
                            <td><%=category.getUserId()%>
                            </td>
                            <td><%=category.getName()%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
