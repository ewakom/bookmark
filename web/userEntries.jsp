<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Entry" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - User Entries</title>

</head>
<body>

<%@include file="layout/header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div style="margin: auto; color: darkgreen">
                    <h1>ENTRIES</h1>
                    <br/>
                </div>
                <div class="table-responsive-lg">
                    <table class="table table-striped table-dark overflow-hidden">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">description</th>
                            <th scope="col">web address</th>
                            <th scope="col">deadline</th>
                            <th scope="col">done</th>
                            <th scope="col">createDate</th>
                            <th scope="col">priority</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            List<Entry> entriesList = (List<Entry>) request.getAttribute("userEntries");
                            int i = 0;
                            for (Entry entry : entriesList) {
                                i++;
                        %>
                        <tr>
                            <th scope="row"><%=i%>
                            </th>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">
                                <%=entry.getDescription()%>
                            </td>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">
                                <a href="<%=entry.getWebAddress()%>"><%=entry.getWebAddress()%>
                                </a>
                            </td>
                            <td><%=entry.getDeadline()%>
                            </td>
                            <td><%=entry.isDone()%>
                            </td>
                            <td><%=entry.getCreateDate()%>
                            </td>
                            <td><%=entry.getPriority()%>
                            </td>
                            <td>
                                <a
                                        class="btn btn-danger"
                                        href="delete-entry.do?id=<%=entry.getId()%>">DELETE</a>
                                <a
                                        class="btn btn-info"
                                        href="edit-entry.do?id=<%=entry.getId()%>">EDIT</a>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                    <a href="add-entry.do" class="btn btn-danger">ADD</a>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
