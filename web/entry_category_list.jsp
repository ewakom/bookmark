<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Category" %>
<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.Entry" %>
<%@ page import="com.ewakom.mywork.jee.bookmark.pojos.EntryCategory" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.UUID" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>BOOKMARK - User Entries</title>

</head>
<body>

<%@include file="layout/admin_header.jsp" %>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-11 mx-auto">
                <div style="margin: auto; color: darkgreen">
                    <h1>ENTRY-CATEGORY</h1>
                    <br/>
                </div>
                <div class="table-responsive-lg">
                    <table class="table table-striped table-dark overflow-hidden">
                        <thead>
                        <tr>
                            <th scope="col">User Id</th>
                            <th scope="col">description</th>
                            <th scope="col">web address</th>
                            <th scope="col">category name</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            List<EntryCategory> entryCategoryList = (List<EntryCategory>) request.getAttribute("entryCategoryList");
                            int i = 0;
                            for (EntryCategory entryCategory : entryCategoryList) {
                                i++;
                        %>
                        <tr>
                            </td>
                            <%
                                UUID entryId = entryCategory.getEntryId();
                                List<Entry> entries = (List<Entry>) request.getAttribute("entries");
                                for (Entry entry : entries) {
                                    if (entryId.equals(entry.getId())) {
                            %>
                            <td scope="row"><%=entry.getUserId()%>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;"><%=entry.getDescription()%>
                            </td>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;"><%=entry.getWebAddress()%>
                            </td>
                            <%
                                    }
                                }
                                UUID categoryId = entryCategory.getCategoryId();
                                List<Category> categories = (List<Category>) request.getAttribute("categories");
                                for (Category category : categories) {
                                    if (categoryId.equals(category.getId())) {
                            %>
                            <td><%=category.getName()%>
                            </td>
                            <%
                                }
                            %>
                            <%
                                }
                            %>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>

<%@include file="layout/footer.jsp" %>

</body>
</html>
